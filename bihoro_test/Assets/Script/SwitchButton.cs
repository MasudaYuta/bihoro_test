﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchButton : MonoBehaviour {

    Toggle toggle;

    float changeRed = 1.0f;
    float changeGreen = 1.0f;
    float changeBlue = 1.0f;
    float changeAlpha = 1.0f;

    Image img;
    Image img2;

    [SerializeField]
    protected GameObject _nextButton;
    [SerializeField]
    protected Image _selectImage;
    [SerializeField]
    protected GameObject _imageObject;
    [SerializeField]
    protected GameObject _imageObject2;

    void Start()
    {
        toggle = GetComponent<Toggle>();
    }

    void Update()
    {
        //		Debug.Log(toggle.isOn);
    }

    public void ChangeToggle()
    {
        if (toggle.isOn == false)
        {
            this.GetComponent<Image>().color = new Color(changeRed, changeGreen, changeBlue, changeAlpha);
            _nextButton.SetActive(false);
        }

        if (toggle.isOn == true)
        {
            float changeRed = 0.7f;
            float changeGreen = 0.7f;
            float changeBlue = 0.7f;
            float changeAlpha = 0.7f;
            this.GetComponent<Image>().color = new Color(changeRed, changeGreen, changeBlue, changeAlpha);

            if (_imageObject.GetComponent<Image>() == null)
            {
                img = _imageObject.AddComponent<Image>();
                img.sprite = _selectImage.GetComponent<Image>().sprite;
                img2 = _imageObject2.AddComponent<Image>();
                img2.sprite = _selectImage.GetComponent<Image>().sprite;
            } else {
                img = _imageObject.GetComponent<Image>();
                img.sprite = _selectImage.GetComponent<Image>().sprite;
                img2 = _imageObject2.GetComponent<Image>();
                img2.sprite = _selectImage.GetComponent<Image>().sprite;
            }
            _nextButton.SetActive(true);
        }      
    }
}
