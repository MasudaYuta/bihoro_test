﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranslationLanguageWord3 : MonoBehaviour {

    [SerializeField]
    protected Text _descriptionText;
    [SerializeField]
    protected Text _returnButton;
    [SerializeField]
    protected Text _nextButton;
    [SerializeField]
    protected Text _qrText;
    [SerializeField]
    protected Text _endButton;

	void Start () {
        switch (TranslationLanguage.Language)
        {
            case "JAPANESE":
                TextManager.Init(TextManager.LANGUAGE.JAPANESE);
                break;
            case "ENGLISH":
                TextManager.Init(TextManager.LANGUAGE.ENGLISH);
                break;
            case "KOREAN":
                TextManager.Init(TextManager.LANGUAGE.KOREAN);
                break;
            case "SIMPLIFIEDCHINESE":
                TextManager.Init(TextManager.LANGUAGE.SIMPLIFIEDCHINESE);
                break;
            case "TRADITIONALCHINESE":
                TextManager.Init(TextManager.LANGUAGE.TRADITIONALCHINESE);
                break;
        }
    }
	
	// Update is called once per frame
	void Update () {
        Transform tf1 = _descriptionText.transform;
        tf1.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.PICTURE);

        Transform tf2 = _returnButton.transform;
        tf2.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.RESHOOT);

        Transform tf3 = _nextButton.transform;
        tf3.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.DECIDETHIS);

        Transform tf4 = _qrText.transform;
        tf4.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.QRCODE);

        Transform tf5 = _endButton.transform;
        tf5.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.END);
    }
}
