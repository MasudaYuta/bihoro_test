﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchImageMaterial : MonoBehaviour {

    Image img;

    [SerializeField]
    protected GameObject _imageMaterialObject;
    [SerializeField]
    protected Image _selectImageMaterial;
    [SerializeField]
    protected GameObject _imageObject;

	void Start () {
        if (_imageObject.GetComponent<Image>() == null)
        {
            img = _imageObject.AddComponent<Image>();
            img.sprite = _selectImageMaterial.GetComponent<Image>().sprite;
        }
        else
        {
            img = _imageObject.GetComponent<Image>();
            img.sprite = _selectImageMaterial.GetComponent<Image>().sprite;
        }
    }

    void Update()
    {
        if (_imageObject.GetComponent<Image>() == null)
        {
            img = _imageObject.AddComponent<Image>();
            img.sprite = _selectImageMaterial.GetComponent<Image>().sprite;
        }
        else
        {
            img = _imageObject.GetComponent<Image>();
            img.sprite = _selectImageMaterial.GetComponent<Image>().sprite;
        }
    }
}