﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackSceneTest : MonoBehaviour {

    public static bool return_flag = false;
    private string[] GameObjectArray = { "DontDestroyObjectManager", "SceneNavigator" };
    private string[] SceneArray = { "CameraChromakeyTest1", "BackImageSelectTest1" };

    public void OnClickBackSceneButton()
    {
        StartCoroutine("LoadBackScene");
    }

    IEnumerator LoadBackScene()
    {
        yield return new WaitForSeconds(0.5f);
        return_flag = true;
        SceneManager.LoadScene(SceneArray[0]);
    }

    public void OnClickEndSceneButton()
    {
        StartCoroutine("LoadEndScene");
    }

    IEnumerator LoadEndScene()
    {
        yield return new WaitForSeconds(1.0f);
        GameObject obj = GameObject.Find(GameObjectArray[0]);
        GameObject obj_scene = GameObject.Find(GameObjectArray[1]);
        Destroy(obj);
        Destroy(obj_scene);
        Debug.Log(DontDestroyObjectManager.Instance);
        SceneManager.LoadScene(SceneArray[1]);
    }
}
