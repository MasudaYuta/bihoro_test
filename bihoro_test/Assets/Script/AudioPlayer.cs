﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioPlayer : MonoBehaviour {

    public AudioSource audioSource;
	
    public void PlaySE()
    {
        audioSource.Play();
    }

}