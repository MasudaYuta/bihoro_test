﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackImageColorChange : MonoBehaviour {

    [SerializeField]
    protected Toggle _switch;
    [SerializeField]
    protected Image _backImage;

	void Update () {
		if(_switch.isOn == true)
        {
            _backImage.color = Color.cyan;
        }
        else{
            _backImage.color = Color.white;
        }
	}
}
