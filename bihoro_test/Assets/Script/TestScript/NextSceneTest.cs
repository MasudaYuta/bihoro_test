﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextSceneTest : MonoBehaviour {

    [SerializeField]
    protected GameObject _saveField1;
    [SerializeField]
    protected GameObject _saveField2;
    [SerializeField]
    protected GameObject _saveObject;

    private string BackUpObject = "BackImageSelect";
    private string LoadScene = "CameraChromakeyTest1";

    public void OnClickNextSceneButton()
    {
        StartCoroutine("ChangeScene");
    }

    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(2.0f);
        DontDestroyObjectManager.Instance.Set(_saveField1, BackUpObject);
        DontDestroyObjectManager.Instance.Set(_saveField2, BackUpObject);
        SceneManager.LoadScene(LoadScene);
    }
}
