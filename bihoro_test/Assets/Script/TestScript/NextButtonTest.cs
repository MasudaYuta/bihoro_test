﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NextButtonTest : MonoBehaviour {

    private string[] _pageArray = { "View2_ja", "View2_en", "View2_ko", "View2_cs", "View2_ct" };

    [SerializeField]
    protected Button _nextButton;
    [SerializeField]
    protected GameObject _view1; //現在の画面
    [SerializeField]
    protected GameObject _view2; //次の画面
    [SerializeField]
    protected GameObject _parentGameObject; //ページを束ねているGameObject

    private string _sceneName = "CameraChromakeyTest1";
    private string _gameObjectName = "View1";

    public void OnClickTransitionButton()
    {
        if (_view1 == GameObject.Find(_gameObjectName) && SceneManager.GetActiveScene().name == _sceneName)
        {
            _nextButton.gameObject.SetActive(false);
        }
        _view1.SetActive(false);
        _view2.SetActive(true);
    }

    public void OnClickTransitionBackButton()
    {
        _view1.SetActive(true);
        _view2.SetActive(false);
    }

    public void OnClickSelectLanguagePage()
    {
        _view1.SetActive(false);
        switch (TranslationLanguage.Language)
        {
            case "JAPANESE":
                _parentGameObject.transform.Find(_pageArray[0]).gameObject.SetActive(true);
                break;
            case "ENGLISH":
                _parentGameObject.transform.Find(_pageArray[1]).gameObject.SetActive(true);
                break;
            case "KOREAN":
                _parentGameObject.transform.Find(_pageArray[2]).gameObject.SetActive(true);
                break;
            case "SIMPLIFIEDCHINESE":
                _parentGameObject.transform.Find(_pageArray[3]).gameObject.SetActive(true);
                break;
            case "TRADITIONALCHINESE":
                _parentGameObject.transform.Find(_pageArray[4]).gameObject.SetActive(true);
                break;
        }
    }
}
