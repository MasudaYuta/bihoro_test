﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnDecition : MonoBehaviour {

    [SerializeField]
    protected GameObject _view1;
    [SerializeField]
    protected GameObject _view2;

	// Use this for initialization
	void Start () {
        if(BackScene.return_flag == true)
        {
            _view1.SetActive(false);
            _view2.SetActive(true);
            BackScene.return_flag = false;
        }
	}
}
