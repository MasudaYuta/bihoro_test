﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SwipeImage : MonoBehaviour {

    [SerializeField]
    protected Text _cautionText;
    [SerializeField]
    protected GameObject _upButton;
    [SerializeField]
    protected GameObject _downButton;

    public float y;
    bool push = false;

    public void PushDown()
    {
        push = true;
    }

    public void PushUp()
    {
        push = false;
    }

    void Update()
    {
        if (push)
        {
            Move();
        }
    }

    public void Move()
    {
        Vector3 pos = _cautionText.transform.position;
        if (pos.y >= 90 && pos.y <= 200)
        {
            _cautionText.transform.position += new Vector3(0, y * Time.deltaTime, 0);
        }
        else if (pos.y < 90)
        {
            _cautionText.transform.position = new Vector3(pos.x, 91, 0);
        }
        else if (pos.y > 200)
        {
            _cautionText.transform.position = new Vector3(pos.x, 199, 0);
        }
    }
}
