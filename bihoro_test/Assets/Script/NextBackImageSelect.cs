﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NextBackImageSelect : MonoBehaviour {

    [SerializeField]
    protected Button _changeButton;
    [SerializeField]
    protected GameObject _parentMaterialObject;

    private int minNumber = 1;
    private int maxNumber = 3;
    private int separateNumber = 19;
    private GameObject _activeObject;
    private GameObject _nextActiveObject;

    public void OnClickNextImage()
    {

        string childObject = _parentMaterialObject.transform.GetComponentInChildren<CanvasRenderer>().name; //activeの子オブジェクトを参照
        string objectName = childObject.Substring(0, separateNumber);
        int number = Int32.Parse(childObject.Substring(separateNumber));
        _activeObject = GameObject.Find(objectName + number);
        if (number != maxNumber)
        {
            number += 1;
        }
        else
        {
            number = minNumber;
        }
        _nextActiveObject = _parentMaterialObject.transform.Find(objectName + number).gameObject;
        _activeObject.SetActive(false);
        _nextActiveObject.SetActive(true);

    }

    public void OnClickPreviousImage()
    {

        string childObject = _parentMaterialObject.transform.GetComponentInChildren<CanvasRenderer>().name;
        string objectName = childObject.Substring(0, separateNumber);
        int number = Int32.Parse(childObject.Substring(separateNumber));
        _activeObject = GameObject.Find(objectName + number);
        if (number != minNumber)
        {
            number -= 1;
        }
        else
        {
            number = maxNumber;
        }
        _nextActiveObject = _parentMaterialObject.transform.Find(objectName + number).gameObject;
        _activeObject.SetActive(false);
        _nextActiveObject.SetActive(true);

    }
}
