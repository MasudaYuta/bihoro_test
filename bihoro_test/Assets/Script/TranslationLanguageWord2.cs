﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranslationLanguageWord2 : MonoBehaviour {

    [SerializeField]
    protected Text _shotButton;

	void Start () {
        switch(TranslationLanguage.Language)
        {
            case "JAPANESE":
                TextManager.Init(TextManager.LANGUAGE.JAPANESE);
                break;
            case "ENGLISH":
                TextManager.Init(TextManager.LANGUAGE.ENGLISH);
                break;
            case "KOREAN":
                TextManager.Init(TextManager.LANGUAGE.KOREAN);
                break;
            case "SIMPLIFIEDCHINESE":
                TextManager.Init(TextManager.LANGUAGE.SIMPLIFIEDCHINESE);
                break;
            case "TRADITIONALCHINESE":
                TextManager.Init(TextManager.LANGUAGE.TRADITIONALCHINESE);
                break;
        }
	}
	
	void Update () {
        Transform tf1 = _shotButton.transform;
        tf1.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.SHOOT);
    }
}
