﻿//  DontDestroyObjectManager.cs
//  http://kan-kikuchi.hatenablog.com/entry/DontDestroyObjectManager
//
//  Created by kan.kikuchi on 2017.05.19.

using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// 特定のシーンでだけ破棄されないオブジェクトを管理するクラス
/// </summary>
public class DontDestroyObjectManager : SingletonMonoBehaviour<DontDestroyObjectManager>
{

    //破棄されないオブジェクトを管理するためのディクショナリー、List<string>で破棄されないシーン名を管理
    private Dictionary<GameObject, List<string>> _objectDict = new Dictionary<GameObject, List<string>>();

    //=================================================================================
    //初期化
    //=================================================================================

    /// <summary>
    /// 初期化(Awake時かその前の初アクセス時、どちらかの一度しか行われない)
    /// </summary>
    protected override void Init()
    {
        base.Init();

        //このオブジェクト自体は永久に破棄しないように
        DontDestroyOnLoad(gameObject);

        //フェードアウトされた時(シーン遷移前)に実行されるようにメソッド登録
        SceneNavigator.Instance.FadeOutFinished += OnFadeOut;
    }

    //=================================================================================
    //設定
    //=================================================================================

    /// <summary>
    /// 特定のシーンでだけ破棄されないオブジェクトを設定、validSceneNamesで破棄されないシーン名を指定
    /// </summary>
    public void Set(GameObject target, params string[] validSceneNames)
    {
        Set(target, new List<string>(validSceneNames));
    }

    /// <summary>
    /// 特定のシーンでだけ破棄されないオブジェクトを設定、validSceneNameListで破棄されないシーン名を指定
    /// </summary>
    public void Set(GameObject target, List<string> validSceneNameList)
    {
        //渡されたオブジェクトを子に配置して、破棄されないように
        target.transform.SetParent(transform);

        //オブジェクトをKeyにして、破棄されないシーン名を登録
        _objectDict[target] = new List<string>(validSceneNameList);
    }

    //=================================================================================
    //破棄
    //=================================================================================

    //フェードアウトされた時に実行される
    private void OnFadeOut()
    {
        //遷移先のシーン名取得
        string nextSceneName = SceneNavigator.Instance.NextSceneName;

        //遷移先で必要ないオブジェクトは破棄し、新たにディクショナリーを生成
        Dictionary<GameObject, List<string>> objectDict = new Dictionary<GameObject, List<string>>();
        foreach (KeyValuePair<GameObject, List<string>> pair in _objectDict)
        {

            //有効なシーン名に遷移先のシーンが含まれていれば保持、でなければ破棄
            if (pair.Value.Contains(nextSceneName))
            {
                objectDict[pair.Key] = pair.Value;
            }
            else
            {
                Destroy(pair.Key);
            }

        }

        _objectDict = objectDict;
    }

}
