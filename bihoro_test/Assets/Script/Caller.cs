﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caller : MonoBehaviour {

    private static string JAVA_CLASS_NAME = "com.plugin.unitycamera.NativeMethod";
    private static string JAVA_METHOD_NAME = "onCreate";
    private static string COROUTINES_NAME = "Sleep";
    private static int WAIT_SECOND = 5;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(COROUTINES_NAME);
    }

    //「コルーチン」で呼び出すメソッド
    IEnumerator Sleep()
    {
        yield return new WaitForSeconds(WAIT_SECOND);
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic(JAVA_METHOD_NAME);
        }
    }
}
