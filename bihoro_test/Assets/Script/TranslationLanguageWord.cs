﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranslationLanguageWord : MonoBehaviour {
    [SerializeField]
    protected Text _startButton;
    [SerializeField]
    protected Text _titleText;
    [SerializeField]
    protected Text _appTitleText1;
    [SerializeField]
    protected Text _pageTitleText1;
    [SerializeField]
    protected Text _nextButton1;
    [SerializeField]
    protected Text _appTitleText2;
    [SerializeField]
    protected Text _pageTitleText2;
    [SerializeField]
    protected Text _nextButton2;
    [SerializeField]
    protected Text _appTitleText3;
    [SerializeField]
    protected Text _descriptionText;
    [SerializeField]
    protected Text _nextButton3;
    [SerializeField]
    protected Text _topText;
    [SerializeField]
    protected Text _returnButton;
    [SerializeField]
    protected Text _nextButton4;
    [SerializeField]
    protected Text _descriptionText2;
    [SerializeField]
    protected Text _noneText;
    [SerializeField]
    protected Text _nextButton5;
    [SerializeField]
    protected Text _topText2;
    [SerializeField]
    protected Text _returnButton2;
    [SerializeField]
    protected Text _nextButton6;


    void Start()
    {
        TextManager.Init(TextManager.LANGUAGE.JAPANESE);
    }
    void Update()
    {
        Transform tf1 = _titleText.transform;
        tf1.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.BIHORO);

        Transform tf2 = _startButton.transform;
        tf2.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.START);

        Transform tf3 = _appTitleText1.transform;
        tf3.gameObject.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.BIHORO);

        Transform tf4 = _pageTitleText1.transform;
        tf4.gameObject.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.POLICY);

        Transform tf5 = _nextButton1.transform;
        tf5.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.CONFIRMATION);

        Transform tf6 = _appTitleText2.transform;
        tf6.gameObject.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.BIHORO);

        Transform tf7 = _pageTitleText2.transform;
        tf7.gameObject.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.POLICY);

        Transform tf8 = _nextButton2.transform;
        tf8.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.AGREEMENT);

        Transform tf9 = _appTitleText3.transform;
        tf9.gameObject.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.BIHORO);

        Transform tf10 = _descriptionText.transform;
        tf10.gameObject.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.BACKGROUND);

        Transform tf11 = _nextButton3.transform;
        tf11.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.SELECT);

        Transform tf12 = _topText.transform;
        tf12.gameObject.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.OK);

        Transform tf13 = _returnButton.transform;
        tf13.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.AGAIN);

        Transform tf14 = _nextButton4.transform;
        tf14.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.DECIDE);

        Transform tf15 = _descriptionText2.transform;
        tf15.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.MATERIAL);

        Transform tf16 = _noneText.transform;
        tf16.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.NONE);

        Transform tf17 = _nextButton5.transform;
        tf17.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.DECIDE);

        Transform tf18 = _topText2.transform;
        tf18.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.MATERIALOK);

        Transform tf19 = _returnButton2.transform;
        tf19.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.AGAIN);

        Transform tf20 = _nextButton6.transform;
        tf20.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.DECIDE);
    }
}