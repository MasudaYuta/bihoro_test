﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.IO;
using System;

public class Server : CreateQRCode {

    private string filePathURL = "C://workspace/03.BihoroProject/99.development/bihoro_test/Assets/TestScreenShot";
    private string postURL = "http://153.121.44.56/www_masuda_y/upload.php";

    void Start()
    {
        StartCoroutine(UploadFile());
    }

    IEnumerator UploadFile()
    {
        string fileName = "Screenshot.png";
        string filePath = null;

        if (Application.platform == RuntimePlatform.Android)
        {
            filePath = Application.persistentDataPath + "/" + fileName; //デバイス用
        }
        else if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            filePath = filePathURL + "/" + fileName;
        }

        //QRコード生成用のリネーム処理
        //fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + fileName;
        fileName = StringUtils.GeneratePassword(20);

        // 画像ファイルをbyte配列に格納
        byte[] img = File.ReadAllBytes(filePath);

        // formにバイナリデータを追加
        WWWForm form = new WWWForm();
        form.AddBinaryData("file", img, fileName, "image/png");
        // HTTPリクエストを送る
        Debug.Log(form.data);
        UnityWebRequest request = UnityWebRequest.Post(postURL, form);
        yield return request.SendWebRequest();

        if (GetRequest(request).isNetworkError)
        {
            // POSTに失敗した場合，エラーログを出力
            Debug.Log(request.error);
        }
        else
        {
            // POSTに成功した場合，レスポンスコードを出力
            Debug.Log(request.responseCode);
            File.Delete(filePath);
            StartCoroutine(CreateQR(fileName));
        }
    }

    private static UnityWebRequest GetRequest(UnityWebRequest request)
    {
        return request;
    }

    public static class StringUtils
    {
        private const string PASSWORD_CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static string GeneratePassword(int length)
        {
            var sb = new System.Text.StringBuilder(length);
            var r = new System.Random();

            for (int i=0; i<length; i++)
            {
                int pos = r.Next(PASSWORD_CHARS.Length);
                char c = PASSWORD_CHARS[pos];
                sb.Append(c);
            }

            return sb.ToString();
        }
    }
}
