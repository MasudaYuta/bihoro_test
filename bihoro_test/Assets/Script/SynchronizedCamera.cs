﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SynchronizedCamera : MonoBehaviour {

    [SerializeField]
    private int m_width = 1920;
    [SerializeField]
    private int m_height = 1080;
    [SerializeField]
    protected Text _text;
    [SerializeField]
    private RawImage displayUI = null;
    [SerializeField]
    protected Text _text2;
    [SerializeField]
    protected Text _text3;
    [SerializeField]
    protected Text _text4;
    [SerializeField]
    protected Text _text5;

    private WebCamTexture webCamTexture = null;



    private IEnumerator Start()
    {
        if (WebCamTexture.devices.Length == 0)
        {
            _text.text = "カメラのデバイスが無い様だ。撮影は諦めよう。";
            yield break;
        }

        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            _text.text = "カメラを使うことが許可されていないようだ。市役所に届けでてくれ！";
            yield break;
        }

        // とりあえず最初に取得されたデバイスを使ってテクスチャを作りますよ。
        WebCamDevice[] devices = WebCamTexture.devices;
        _text2.text = devices.Length + "つのデバイスが接続中";
        if (devices.Length > 0)
            for(int i=0;i < devices.Length; i++)
            {
                if (i == 0)
                {
                    _text3.text = devices[i].name;
                }
                if (i == 1)
                {
                    _text4.text = devices[i].name;
                }
                if (i == 2)
                {
                    _text5.text = devices[i].name;
                }
            }
        else
            _text.text = "カメラのデバイスが無い様だ。撮影は諦めよう。";
        WebCamDevice userCameraDevice = WebCamTexture.devices[0];
        webCamTexture = new WebCamTexture(userCameraDevice.name, m_width, m_height);

        displayUI.texture = webCamTexture;

        // さあ、撮影開始だ！
        webCamTexture.Play();
    }
}
