﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageMaterial : MonoBehaviour {

    [SerializeField]
    protected GameObject _imageObject;
    [SerializeField]
    protected GameObject _imageMaterialObject;

    private string SelectedObject = "SelectBackImageArea";
    private string SelectedMaterial = "SelectMaterialArea";

	// Use this for initialization
	void Start () {
        Texture selectedObject = GameObject.Find(SelectedObject).GetComponent<Image>().mainTexture;
        _imageObject.GetComponent<Renderer>().material.mainTexture = selectedObject;

        Sprite selectedMaterial = GameObject.Find(SelectedMaterial).GetComponent<Image>().sprite;
        _imageMaterialObject.GetComponent<Image>().sprite = selectedMaterial;
    }
}
