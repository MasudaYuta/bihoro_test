﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class CreateQRCode : MonoBehaviour {


    public enum QRImageSize
    {
        SIZE_128 = 7,
        SIZE_256,
        SIZE_512,
        SIZE_1024,
        SIZE_2048,
        SIZE_4096
    }

    public static IEnumerator CreateQR(string fileName)
    {
        QRImageSize _size = QRImageSize.SIZE_256;
        string savePath = "";

        if (Application.platform == RuntimePlatform.Android)
        {
            savePath = Application.persistentDataPath + "/qr.png"; //デバイス用
        }
        else if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            savePath = "C://workspace/03.BihoroProject/99.development/bihoro_test/Assets/TestScreenShot/qr.png";
        }
        string _content = "http://153.121.44.56/www_masuda_y/upload/" + fileName;
        int size = (int)Mathf.Pow(2f, (int)_size);
        var tex = QRCodeHelper.CreateQRCode(_content, size, size);
        using (var fs = new FileStream(savePath, FileMode.OpenOrCreate))
        {
            var b = tex.EncodeToPNG();
            fs.Write(b, 0, b.Length);
        }

        // ファイルが無かったらやめる
        if (!System.IO.File.Exists(savePath))
        {
            Debug.Log("File does NOT exist!!, path = " + savePath);
            yield break;
        }

        const string _FILE_HEADER = "file://";

        // ファイル読み込み
        WWW request = new WWW(_FILE_HEADER + savePath);

        // 読み込み待ち
        while (!request.isDone)
        {
            yield return new WaitForEndOfFrame();
        }

        yield return request;

        RawImage rawImage = GameObject.Find("QRCodeImage").GetComponent<RawImage>();
        rawImage.texture = request.textureNonReadable;
        File.Delete(savePath);
        yield return 0;
    }
}
