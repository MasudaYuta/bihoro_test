﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranslationLanguage : MonoBehaviour {
    [SerializeField]
    protected Button _languageButton;
    private string[] _languageArray = { "JAPANESE", "ENGLISH", "KOREAN", "SIMPLIFIEDCHINESE", "TRADITIONALCHINESE" };
    public static string Language;

    private void Start()
    {
        Language = _languageArray[0];  
    }

    public void OnClickTranslationJapanese()
    {
        TextManager.Init(TextManager.LANGUAGE.JAPANESE);
        Language = _languageArray[0];
    }

    public void OnClickTranslationEnglish()
    {
        TextManager.Init(TextManager.LANGUAGE.ENGLISH);
        Language = _languageArray[1];
    }

    public void OnClickTranslationKorean()
    {
        TextManager.Init(TextManager.LANGUAGE.KOREAN);
        Language = _languageArray[2];
    }

    public void OnClickTranslationSimplifiedChinese()
    {
        TextManager.Init(TextManager.LANGUAGE.SIMPLIFIEDCHINESE);
        Language = _languageArray[3];
    }

    public void OnClickTranslationTraditionalChinese()
    {
        TextManager.Init(TextManager.LANGUAGE.TRADITIONALCHINESE);
        Language = _languageArray[4];
    }
}
