﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour {
    private float _maxTime = 6; //カウント数+1
    private float _timeCount;

    [SerializeField]
    protected Text _countDownText;
    [SerializeField]
    protected GameObject _viewBefore; //今の画面
    [SerializeField]
    protected GameObject _viewAfter; //次の画面

    void Start()
    {
        _timeCount = _maxTime;
        _countDownText.text = ((int)_timeCount).ToString();
    }

    void Update()
    {
        _timeCount -= Time.deltaTime;
        _countDownText.text = ((int)_timeCount).ToString();
        if (_timeCount <= 1)
        {
            _viewBefore.SetActive(false);
            _viewAfter.SetActive(true);
        }
    }
}
