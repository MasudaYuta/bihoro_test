﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class ScreenShotImg : MonoBehaviour
{

    private RectTransform imageSize;
    private float x, y;
    private string pictureName = "Screenshot.png";
    private string readFileURL = "C://workspace/03.BihoroProject/99.development/bihoro_test/Assets/TestScreenShot/Screenshot.png";

    IEnumerator Start()
    {
        RawImage rawImage = GetComponentInChildren<RawImage>();
        byte[] bytes = null;

        if(Application.platform == RuntimePlatform.Android)
        {
            Debug.Log(Application.persistentDataPath + "/" + pictureName); //デバイス用
            bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + pictureName); //デバイス用
        }
        //Windowsテスト用
        else if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            bytes = File.ReadAllBytes(readFileURL);
        }

        Texture2D texture = new Texture2D(200, 200);
        texture.filterMode = FilterMode.Trilinear;
        texture.LoadImage(bytes);

        rawImage.texture = texture;
        imageSize = rawImage.GetComponent<RectTransform>();
        imageSize.sizeDelta = new Vector2(700, 360);

        yield return null;
    }
}
