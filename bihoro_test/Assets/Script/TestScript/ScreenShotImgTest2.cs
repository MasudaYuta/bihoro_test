﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ScreenShotImgTest2 : MonoBehaviour
{

    private RectTransform imageSize;
    private string saveURL = "C://workspace/03.BihoroProject/99.development/bihoro_test/Assets/TestScreenShot/Screenshot.png";
    private string pictureURL = "Screenshot.png";

    IEnumerator Start()
    {
        RawImage rawImage = GetComponentInChildren<RawImage>();
        byte[] bytes = null;
        byte[] pngData = null;

        if (Application.platform == RuntimePlatform.Android)
        {
            Debug.Log(Application.persistentDataPath + "/" + pictureURL); //デバイス用
            bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + pictureURL); //デバイス用
        }
        else if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            bytes = File.ReadAllBytes(saveURL);
        }

        Texture2D texture = new Texture2D(200, 200);
        texture.filterMode = FilterMode.Bilinear;
        texture.LoadImage(bytes);

        if (BackSceneTest.return_flag == true)
        {
            //            Texture2D trimmingImage = getCenterClippedTexture(texture);

            //加工後のデータ保存
            //            pngData = trimmingImage.EncodeToPNG();
            //            if (Application.platform == RuntimePlatform.Android)
            //            {
            //                File.WriteAllBytes(Application.persistentDataPath + "/" + pictureURL, pngData); //デバイス用
            //            }
            //            else if (Application.platform == RuntimePlatform.WindowsEditor)
            //            {
            //                File.WriteAllBytes(saveURL, pngData);
            //            }
            //            rawImage.texture = trimmingImage;
            rawImage.texture = texture;
            BackSceneTest.return_flag = false;
        }
        else
        {
            rawImage.texture = texture;
        }
        imageSize = rawImage.GetComponent<RectTransform>();
        imageSize.sizeDelta = new Vector2(1250, 646);

        yield return null;
    }

    Texture2D getCenterClippedTexture(Texture2D texture)
    {
        Color[] pixel;
        Texture2D clipTex;

        //画面露出部と素材の範囲に合わせて画像の切り取り
        //pixel = texture.GetPixels(60, 30, 1250, 690);
        //clipTex = new Texture2D(1250,690);
        pixel = texture.GetPixels(0, 0, Screen.width*2, Screen.height*2);
        clipTex = new Texture2D(Screen.width*2, Screen.height*2);
        clipTex.SetPixels(pixel);
        clipTex.Apply();
        return clipTex;
    }
}
