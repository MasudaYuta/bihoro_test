﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranslationLanguageWordTest2 : MonoBehaviour {

    void Start()
    {
        switch (TranslationLanguage.Language)
        {
            case "JAPANESE":
                TextManager.Init(TextManager.LANGUAGE.JAPANESE);
                break;
            case "ENGLISH":
                TextManager.Init(TextManager.LANGUAGE.ENGLISH);
                break;
            case "KOREAN":
                TextManager.Init(TextManager.LANGUAGE.KOREAN);
                break;
            case "SIMPLIFIEDCHINESE":
                TextManager.Init(TextManager.LANGUAGE.SIMPLIFIEDCHINESE);
                break;
            case "TRADITIONALCHINESE":
                TextManager.Init(TextManager.LANGUAGE.TRADITIONALCHINESE);
                break;
        }
    }
}
