﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndCountDown : MonoBehaviour {
    private float _maxTime = 61; //カウント数+1
    private float _timeCount;

    [SerializeField]
    protected Text _countDownText;

    void Start()
    {
        _timeCount = _maxTime;
        _countDownText.text = ((int)_timeCount).ToString();
    }

    void Update()
    {
        _timeCount -= Time.deltaTime;
        switch (TranslationLanguage.Language)
        {
            case "JAPANESE":
                _countDownText.text = ((int)_timeCount).ToString() + "秒後にTOPに戻ります";
                break;
            case "ENGLISH":
                _countDownText.text = "After " + ((int)_timeCount).ToString() + " seconds we will return to top screen.";
                break;
            case "KOREAN":
                _countDownText.text = ((int)_timeCount).ToString() + " 초 후 메인 화면으로 돌아갑니다";
                break;
            case "SIMPLIFIEDCHINESE":
                _countDownText.text = "在 " + ((int)_timeCount).ToString() + " 秒内返回到最上面的屏幕";
                break;
            case "TRADITIONALCHINESE":
                _countDownText.text = "在 " + ((int)_timeCount).ToString() + " 秒內返回到最上面的屏幕";
                break;
        }
        if (_timeCount <= 1)
        {
            StartCoroutine("CountDownSound");
        }
    }

    IEnumerator CountDownSound()
    {
        yield return new WaitForSeconds(1.5f);
        GameObject obj = GameObject.Find("DontDestroyObjectManager");
        GameObject obj_scene = GameObject.Find("SceneNavigator");
        Destroy(obj);
        Destroy(obj_scene);
        SceneManager.LoadScene("BackImageSelect");
    }
}
