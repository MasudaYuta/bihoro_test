﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranslationLanguageWordTest : MonoBehaviour {

    [SerializeField]
    protected Text _noneText;

    void Start()
    {
        TextManager.Init(TextManager.LANGUAGE.JAPANESE);
    }
    void Update()
    {
        Transform tf5 = _noneText.transform;
        tf5.GetComponent<Text>().text
            = TextManager.Get(TextManager.KEY.NONE);
    }
}
