﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class ScreenShot : MonoBehaviour{

    /// <summary>
    /// スクショを撮影
    /// </summary>
    void Start()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            StartCoroutine(
              ScreenShotCaptor.Capture(
                imageName: "C://workspace/03.BihoroProject/99.development/bihoro_test/Assets/TestScreenShot/Screenshot.png",
                callback: Callback
              )
            );
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            StartCoroutine(
              ScreenShotCaptor.Capture(
                imageName: "Screenshot.png",
                callback: Callback
              )
            );
        }

    }

    //撮影完了時に実行される
    private void Callback()
    {
        StartCoroutine("SoundWait");
    }

    IEnumerator SoundWait()
    {
        yield return new WaitForSeconds(1.8f);
        SceneManager.LoadScene("ScreenShotSelect");
    }
}
