﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenShotTest : MonoBehaviour {

    private string imageNameURL = "C://workspace/03.BihoroProject/99.development/bihoro_test/Assets/TestScreenShot/Screenshot.png";
    private string pictureName = "Screenshot.png";
    private string LoadScene = "ScreenShotSelectTest1";

    /// <summary>
    /// スクショを撮影
    /// </summary>
    void Start()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            StartCoroutine(
              ScreenShotCaptor.Capture(
                imageName: imageNameURL,
                callback: Callback
              )
            );
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            StartCoroutine(
              ScreenShotCaptor.Capture(
                imageName: pictureName,
                callback: Callback
              )
            );
        }

    }

    //撮影完了時に実行される
    private void Callback()
    {
        SceneManager.LoadScene(LoadScene);
    }
}
