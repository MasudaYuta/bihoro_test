﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScene : MonoBehaviour {

    [SerializeField]
    protected GameObject _saveField;
    [SerializeField]
    protected GameObject _saveObject;

    public void OnClickNextSceneButton()
    {
        StartCoroutine("ChangeScene");
    }

    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(2.0f);
        DontDestroyObjectManager.Instance.Set(_saveField, "BackImageSelect");
        SceneManager.LoadScene("CameraChromakey");
    }
}
