﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndCountDownTest : MonoBehaviour {

    private float _maxTime = 61; //カウント数+1
    private float _timeCount; //カウント用変数
    private float _changeDescriptionTime = 3; //文言変更の秒数
    private float _changeDescriptionTimeCount; //文言変更用カウント変数

    private string[] gameObjectArray = { "DontDestroyObjectManager", "SceneNavigator" };
    private string scene = "BackImageSelectTest1";

    private bool _changeTimeText = false;

    [SerializeField]
    protected Text _descriptionText;

    void Start()
    {
        _timeCount = _maxTime; 
        _descriptionText.text = TextManager.Get(TextManager.KEY.QRCODE); //カウントダウンの秒数をテキストに追記

        _changeDescriptionTimeCount = _changeDescriptionTime;
    }

    void Update()
    {
        _timeCount -= 1 * Time.deltaTime;
        _changeDescriptionTimeCount -= 1 * Time.deltaTime;

        if (_changeTimeText == false)
        {
            //カウントダウンテキストの表示
            switch (TranslationLanguage.Language)
            {
                case "JAPANESE":
                case "KOREAN":
                    _descriptionText.text = ((int)_timeCount).ToString() + " " + TextManager.Get(TextManager.KEY.DESCRIPTION_BEHIND);
                    break;
                case "ENGLISH":
                case "SIMPLIFIEDCHINESE":
                case "TRADITIONALCHINESE":
                    _descriptionText.text = TextManager.Get(TextManager.KEY.DESCRIPTION_AVOBE) + " " +  ((int)_timeCount).ToString() + " " + TextManager.Get(TextManager.KEY.DESCRIPTION_BEHIND);
                    break;
            }
        }
        else
        {
            //説明テキストの表示
            _descriptionText.text = TextManager.Get(TextManager.KEY.QRCODE);
        }

        if (_changeDescriptionTimeCount <= 0)
        {
            if (_changeTimeText == false)
            {
                _changeTimeText = true;
            }
            else
            {
                _changeTimeText = false;
            }
            _changeDescriptionTimeCount = _changeDescriptionTime;
        } 

        if (_timeCount <= 1)
        {
            StartCoroutine("CountDownSound");
        }
    }

    IEnumerator CountDownSound()
    {
        yield return new WaitForSeconds(1.5f);
        GameObject obj = GameObject.Find(gameObjectArray[0]);
        GameObject obj_scene = GameObject.Find(gameObjectArray[1]);
        Destroy(obj);
        Destroy(obj_scene);
        SceneManager.LoadScene(scene);
    }
}
