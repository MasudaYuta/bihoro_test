﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchImageMaterialTest : MonoBehaviour {

    Image img;
    Image img2;

    [SerializeField]
    protected GameObject _imageMaterialObject;
    [SerializeField]
    protected Image _selectImageMaterial;
    [SerializeField]
    protected GameObject _imageObject;

    void Start()
    {
        if (_imageObject.GetComponent<Image>() == null)
        {
            img = _imageObject.AddComponent<Image>();
            img2 = _imageMaterialObject.AddComponent<Image>();
        }
        else
        {
            img = _imageObject.GetComponent<Image>();
            img2 = _imageMaterialObject.GetComponent<Image>();
        }

        img.sprite = _selectImageMaterial.GetComponent<Image>().sprite;
        img2.sprite = _selectImageMaterial.GetComponent<Image>().sprite;
    }

    void Update()
    {
        if (_imageObject.GetComponent<Image>() == null)
        {
            img = _imageObject.AddComponent<Image>();
            img2 = _imageMaterialObject.AddComponent<Image>();
        }
        else
        {
            img = _imageObject.GetComponent<Image>();
            img2 = _imageMaterialObject.GetComponent<Image>();
        }
        img.sprite = _selectImageMaterial.GetComponent<Image>().sprite;
        img2.sprite = _selectImageMaterial.GetComponent<Image>().sprite;
    }
}
