﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnDecitionTest : MonoBehaviour {

    private string[] ViewNumberArray = {"View1", "View2"};
    private string[] LanguageArray = { "_ja", "_en", "_ko", "_cs", "_ct" };
    private string ViewObject;
    private string ViewNumber;
    private string Language;

    [SerializeField]
    protected GameObject _parentGameObject;

    // Use this for initialization
    void Start()
    {
        ViewNumber = ViewNumberArray[0];
        ViewObject = ReturnViewObject(ViewNumber);

        if (BackSceneTest.return_flag == true)
        {
            _parentGameObject.transform.Find(ViewObject).gameObject.SetActive(false);
            ViewNumber = ViewNumberArray[1];
            ViewObject = ReturnViewObject(ViewNumber);
        }

        _parentGameObject.transform.Find(ViewObject).gameObject.SetActive(true);
    }

    string ReturnViewObject(string ViewNumber)
    {
        switch (TranslationLanguage.Language)
        {
            case "JAPANESE":
                Language = LanguageArray[0];
                break;
            case "ENGLISH":
                Language = LanguageArray[1];
                break;
            case "KOREAN":
                Language = LanguageArray[2];
                break;
            case "SIMPLIFIEDCHINESE":
                Language = LanguageArray[3];
                break;
            case "TRADITIONALCHINESE":
                Language = LanguageArray[4];
                break;
        }
        return ViewNumber + Language;
    } 
}
