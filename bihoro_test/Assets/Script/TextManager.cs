﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 文字列管理クラス(多言語対応)
/// </summary>
public static class TextManager
{
    // 文字列格納、検索用ディクショナリー
    private static Dictionary<string, string> sDictionary = new Dictionary<string, string>();

    /// <summary>
    /// 検索キー
    /// </summary>
    public enum KEY
    {
        BIHORO,
        START,
        POLICY,
        CONFIRMATION,
        AGREEMENT,
        BACKGROUND,
        SELECT,
        OK,
        AGAIN,
        DECIDE,
        SHOOT,
        PICTURE,
        RESHOOT,
        DECIDETHIS,
        QRCODE,
        RETURN,
        END,
        MATERIAL,
        NONE,
        MATERIALOK,
        PLAY,
        DESCRIPTION_AVOBE,
        DESCRIPTION_BEHIND
    };

    /// <summary>
    /// 使用言語
    /// </summary>
    public enum LANGUAGE
    {
        JAPANESE,
        ENGLISH,
        KOREAN,
        SIMPLIFIEDCHINESE,
        TRADITIONALCHINESE
    }

    /// <summary>
    /// 文字列初期化
    /// </summary>
    /// <param name="lang">使用言語</param>
    public static void Init(LANGUAGE lang)
    {
        // リソースファイルパス決定
        string filePath;
        if (lang == LANGUAGE.JAPANESE)
        {
            filePath = "Text/japanese";
        }
        else if (lang == LANGUAGE.ENGLISH)
        {
            filePath = "Text/english";
        }
        else if (lang == LANGUAGE.KOREAN)
        {
            filePath = "Text/korean";
        }
        else if (lang == LANGUAGE.SIMPLIFIEDCHINESE)
        {
            filePath = "Text/simplifiedchinese";
        }
        else if (lang == LANGUAGE.TRADITIONALCHINESE)
        {
            filePath = "Text/traditionalchinese";
        }
        else
        {
            throw new Exception("TextManager Init failed.");
        }

        // ディクショナリー初期化
        sDictionary.Clear();
        TextAsset csv = Resources.Load<TextAsset>(filePath);
        StringReader reader = new StringReader(csv.text);
        while (reader.Peek() > -1)
        {
            string[] values = reader.ReadLine().Split('\t');
            sDictionary.Add(values[0], values[1].Replace("\\n", "\n"));
        }
    }

    /// <summary>
    /// 文字列取得
    /// </summary>
    /// <param name="key">文字列取得キー</param>
    /// <returns>キーに該当する文字列</returns>
    public static string Get(KEY key)
    {
        return Get(Enum.GetName(typeof(KEY), key));
    }

    /// <summary>
    /// 文字列取得
    /// </summary>
    /// <param name="key">文字列取得キー</param>
    /// <returns>キーに該当する文字列</returns>
    public static string Get(string key)
    {
        return sDictionary[key];
    }
}
