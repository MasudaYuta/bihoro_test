﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenGraphics : MonoBehaviour {

    [SerializeField]
    protected Text _text;

    // Use this for initialization
    void Start()
    {
        int Width = Screen.currentResolution.width;
        int Height = Screen.currentResolution.height;
    }

    // Update is called once per frame
    void Update()
    {
        int Width = Screen.currentResolution.width;
        int Height = Screen.currentResolution.height;

        _text.text = "実行環境の解像度 Width : " + Width + ",Height : " + Height;
    }
}
