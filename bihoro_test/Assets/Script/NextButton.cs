﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NextButton : MonoBehaviour {

    [SerializeField]
    protected Button _nextButton;
    [SerializeField]
    protected GameObject _view1; //現在の画面
    [SerializeField]
    protected GameObject _view2; //次の画面

    private string[] _gameSceneObjectArray = { "BackImageSelect", "CameraChromakey" };
    private string[] _gameObjectArray = { "SelectBackImageArea", "View1", "View5" };


    public void OnClickTransitionButton()
    {

        if (_view1 == GameObject.Find(_gameObjectArray[2]) && SceneManager.GetActiveScene().name == _gameSceneObjectArray[0])
        {
            DontDestroyObjectManager.Instance.Set(GameObject.Find(_gameObjectArray[0]), _gameSceneObjectArray[0]);
        }
        if (_view1 == GameObject.Find(_gameObjectArray[1]) && SceneManager.GetActiveScene().name == _gameSceneObjectArray[1])
        {
            _nextButton.gameObject.SetActive(false);
        }
        _view1.SetActive(false);
        _view2.SetActive(true);
    }

    public void OnClickTransitionBackButton()
    {
        _view1.SetActive(true);
        _view2.SetActive(false);
    } 
}